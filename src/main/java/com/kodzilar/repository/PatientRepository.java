package com.kodzilar.repository;


import com.kodzilar.entity.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    public Patient findByUsername(String username);
}
