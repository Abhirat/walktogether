package com.kodzilar.mail;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component("mailClient")
public class MailClient{

    private JavaMailSender mailSender;
    private MimeMessagePreparator mimeMessage;
    private TemplateEngine templates;

    private static final String TEMPLATE_EMAIL = "inviteTemplate";
    private static final String URL = "signupcaretaker.walktogether.com?id=";

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Autowired
    public MailClient(JavaMailSender mailSender, TemplateEngine templates){
        this.mailSender = mailSender;
        this.templates = templates;
    }

    public void prepareEmail(String recipient,int id){

        mimeMessage = (mimeMessage)-> {
            String message = buildTemplate(id);

            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(recipient);
            messageHelper.setSubject("This is the good chance to help the people.");
            messageHelper.setText(message, true);
        };
    }

    private String buildTemplate(int id){
        Context context = new Context();
        context.setVariable("url", URL+id);
        System.out.println(URL+id);
        return templates.process(TEMPLATE_EMAIL, context);
    }

    public void send(){
        mailSender.send(mimeMessage);
    }
}