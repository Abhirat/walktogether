package com.kodzilar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kodzilar.entity.History;
import com.kodzilar.entity.Mission;
import com.kodzilar.entity.Patient;
import com.kodzilar.entity.Question;
import com.kodzilar.repository.HistoryRepository;
import com.kodzilar.repository.MissionRepository;
import com.kodzilar.repository.PatientRepository;
import com.kodzilar.repository.QuestionRepository;

@Service
public class MissionService {

    @Autowired
    private MissionRepository missionRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private HistoryRepository historyRepository;

    public List<Mission> getMissionByParkName(String parkName) {
        List<Mission> missions = missionRepository.findByParkName(parkName);

        for(Mission mission: missions){
            List<Question> questions = questionRepository.findByMissionId(mission.getMissionId());
            mission.setQusetions(questions);
        }

        return missions;
    }

    public void save(History history){
        Patient patient = patientRepository.findByUsername(history.getPatientName());
        history.setPatientId(patient.getPatientId());
        missionRepository.save(history);
    }

    public List<History> getHistoryById(History history){
        Patient patient = patientRepository.findByUsername(history.getPatientName());
        return historyRepository.findAllByPatientIdOrderByLastMntDateAsc(patient.getPatientId(), history.getLastMntDate());
    }

    public List<History> getDateListById(History history){
        Patient patient = patientRepository.findByUsername(history.getPatientName());
        return historyRepository.findDateByIdOrderByDateAsc(patient.getPatientId());
    }
}