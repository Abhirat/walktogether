package com.kodzilar.service;

import com.kodzilar.entity.Caretaker;
import com.kodzilar.entity.Patient;
import com.kodzilar.repository.CaretakerRepository;
import com.kodzilar.repository.PatientRepository;
import com.kodzilar.utils.StringUtils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private CaretakerRepository caretakerRepository;

    @Autowired
    private PatientRepository patientRepository;

    public HttpStatus loginCaretaker(Caretaker caretaker) {

        try {
            String email = caretaker.getEmail();
            String password = StringUtils.getEncodeSHA512(caretaker.getPassword());

            Caretaker caretakerFromDB = caretakerRepository.findByEmail(email);
            if (null != caretakerFromDB
                    && email.equalsIgnoreCase(caretakerFromDB.getEmail())
                    && password.equalsIgnoreCase(caretakerFromDB.getPassword())) {

                return HttpStatus.OK;
            } else {
                return HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            return HttpStatus.BAD_REQUEST;
        }
    }

    public HttpStatus loginPatient(Patient patient) {

        try {
            String username = patient.getUsername();
            String password = StringUtils.getEncodeSHA512(patient.getPassword());

            Patient patientFromDB = patientRepository.findByUsername(username);
            if (null != patientFromDB
                    && username.equalsIgnoreCase(patientFromDB.getUsername())
                    && password.equalsIgnoreCase(patientFromDB.getPassword())) {

                return HttpStatus.OK;
            } else {
                return HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            return HttpStatus.BAD_REQUEST;
        }
    }

    public HttpStatus registerCaretaker(Caretaker caretaker) {
        String email = caretaker.getEmail();
        String password = caretaker.getPassword();
        try {
            if (null == caretakerRepository.findByEmail(email)) {
                caretaker.setPassword(StringUtils.getEncodeSHA512(password));
                caretakerRepository.save(caretaker);
            } else {
                return HttpStatus.BAD_REQUEST;
            }
        } catch (Exception e) {
            return HttpStatus.BAD_REQUEST;
        }

        return HttpStatus.OK;
    }

    public Patient registerPatient(Patient patient) {
        String username = patient.getUsername();
        String password = patient.getPassword();
        try {
            if (null == patientRepository.findByUsername(username)) {
                patient.setPassword(StringUtils.getEncodeSHA512(password));
                patient = patientRepository.save(patient);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

        return patient;
    }
}
